part of coUserver;

@app.Route('/slack', methods: const [app.POST])
String parseMessageFromSlack(@app.Body(app.FORM) Map form) {
  String token = form['token'];
  if (token != glitchForeverKey) {
    return "NOT AUTHORIZED";
  }

  String username = form['user_name'];
  String text = form['text'];

  if (username != "slackbot" && text != null && text.isNotEmpty) {
    Map map = {'username': username, 'message': text, 'channel': 'Global Chat'};
    ChatHandler.sendAll(JSON.encode(map));
  }

  return "OK";
}
